#mysqliDb.class.php

一、作者

    本人 @闫嵩达 是一位在校大学生。大神可以直接路过~~当然如果代码中如果有什么您一眼就看的出来问题的，还烦请您帮忙指出。
    由于自己对计算机，互联网感兴趣，所以一直在看这方面的知识。现在正在努力提升自己中……
    

二、开发原因

    由于 mysql_query() 函数在php5.5中废弃，在以后的函数中也将移出。所以有了开发这个类库的想法。同时也是为了方便以后自己开发~

三、主要功能

	NOTE: 如果您喜欢，您可以再遵循 GPL 协议的基础上，放心使用本类~
	
    本类主要是将一些常用的sql语句完全数组化，即sql语句全部由多维数组组成，更加方便。
    对数据库的其他主要常用操作，都已经集成在本类中。方便使用~

    sql的所有预定字段：
    	select：model,table,[field,where,limit]
    	update: model,table,where,[key1,key2,……]
    	insert: model,table,[key1,key2,……]

    字段对应的值：
      [model] :(string) 对应的value值为所要进行的操作类型。
      			目前共有三种：select，update，insert；
      [field] :(array) 【仅在select模式下有用】对应要选择的数据库字段
      [table] :(string) 对应的value值为要操作的数据表；
      [where] :(array) 对应的value值为一个数组；
      [limit] :(string) 【仅在select模式下有用】对应sql语句中的limit值。
      			与原sql一致；其它字段为数据库中要操作的字段。

四、demo

	NOTE: 本demo均为使用数组功能的demo。原生sql和原sql一致。
   	A) 实现 SELECT 查询：
		NOTE: 在select中，若要使用数组功能，则必要的数组key有：model,table；
				可选的key有：where,limit,field。若有其它key，则自动忽略。

		include "mysqliDb.class.php";
		$Db = new mysqliDb('localhost', 'root', 'root', 'test');
		$sql = array(
			'model' => 'select',
			'table' => 'user',
			'field' => array('username','id'),
			'where' => array('id' => '1'),
			'limit' => '0,10',
		);
		$Db->getRow($sql);

	B) 实现 INSERT 操作：
		NOTE: 在 insert 模式中，除了规定的字段，其它字段均视为插入数据库的key-		value，同时若有仅限select使用的规定字段，则自动忽略

		include "mysqliDb.class.php";
		$Db = new mysqliDb('localhost', 'root', 'root', 'test');
		$sql = array(
			'model' => 'insert',
			'table' => 'user',
			'username' => 'test1',
			'password' => md5('admin123'),
			);
		$Db->insert_id($sql);

	C) 实现 UPDATE 操作：
		NOTE: 除了规定的 model,table,where 三个字段，
				其它均被是为需要更新的数据库字段。同时若有仅限select使用的规定字段，则自动忽略

		include "mysqliDb.class.php";
		$Db = new mysqliDb('localhost', 'root', 'root', 'test');
		$sql = array(
			'model' => 'update',
			'table' => 'user',
			'where' => array('id' => '1'),
			'username' => 'test1',
			'password' => md5('admin123'),
		);
		$Db->aff($sql);

感谢各位大神的支持和耐心查看。如果您有一些意见，希望您能提出来。我会认真改进。也算是给自己一个提升机会！