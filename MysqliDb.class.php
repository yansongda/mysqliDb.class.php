<?php
/**
 * 属于自己的mysqli公共类
 * ***************************************
 * @author JasonYan <yansong.da@qq.com>
 * @createTime 2014/08/11 13:08
 * @lastModified 2014/08/11 15:28
 * ***************************************
 * 您可以在遵循 GPL 协议的情况下放心地使用本代码~
 * ***************************************
 */

class MysqliDb {

	private $link;

    /**
	 * mysqli初始化函数
	 * @param [type] $dbhost     [数据库服务器]
	 * @param [type] $dbusername [数据库用户名]
	 * @param [type] $dbpwd      [数据库密码]
	 * @param [type] $db         [选择的数据库]
	 * @param string $charset    [字符集。默认为：utf8]
	 */
	public function __construct($dbhost, $dbusername, $dbpwd, $db, $charset = "uft8"){
		$this->link = $this->connect($dbhost, $dbusername, $dbpwd, $db, $charset = "uft8");
	}

	/**
	 * mysqli连接函数
	 * @param [type] $dbhost     [数据库服务器]
	 * @param [type] $dbusername [数据库用户名]
	 * @param [type] $dbpwd      [数据库密码]
	 * @param [type] $db         [选择的数据库]
	 * @param string $charset    [字符集。默认为：utf8]
	 * @return [type]            [成功时返回连接标示。失败是返回错误信息]
	 */
	public function connect($host, $username, $pwd, $db, $charset){
		$theLink = mysqli_connect($host, $username, $pwd, $db);
		if ( !$theLink ) {
			$info = "数据库连接错误。请检查数据库服务器、数据库用户名、数据库密码的正确性！<br/>Debug Info:".mysqli_connect_errno().":".mysqli_connect_error();
			$this->err($info);
		} else {
			mysqli_set_charset($theLink, $charset);
			return $theLink;
		}
	}

	/**
	 * 执行sql语句。
	 * @param  [type] $link [连接标示]
	 * @param  [array || string] $sql  [sql语句。可以是数组，也可以是一条sql]
	 * @return [type]       [description]
	 */
	public function query($sql){
		if ( is_array($sql) ) {
			//组合sql语句前半句
			switch ($sql['model']) {
				case 'insert':
					$sqlNew = "INSERT INTO `". $sql['table'] ."` (";
					//去除sql模式和数据表字段
					if ( isset($sql['where']) ) {
						unset($sql['where']);
					}
					if ( isset($sql['limit']) ) {
						unset($sql['limit']);
					}
					if ( isset($sql['field']) ) {
						unset($sql['field']);
					}
					foreach ($sql as $k => $v) {
						if ( $k == 'model' || $k == 'table' ) {
							continue;
						}
						$s[$k] = $v;
					}
					//循环出字段，去除最后一个多余的 ， 号
					foreach ($s as $key1 => $value1) {

						$sqlNew .= "`".$key1."`,";
					}
					$sqlNew = substr($sqlNew, 0, -1).") VALUES (";
					//循环出value值，去除最后一个 ， 号
					foreach ($s as $key2 => $value2) {
						$sqlNew .= "'".$value2."',";
					}
					//最终新的sql语句
					$sqlNew = substr($sqlNew, 0, -1).")";

					break;
				
				case 'update':
					if ( isset($sql['limit']) ) {
						unset($sql['limit']);
					}
					if ( isset($sql['field']) ) {
						unset($sql['field']);
					}
					$sqlNew = "UPDATE `". $sql['table'] ."` SET";
					foreach ($sql as $k => $v) {
						if ( $k == 'model' || $k == 'table' || $k == 'where') {
							continue;
						}
						$s[$k] = $v;
					}
					//循环出字段，去除最后一个多余的 ， 号
					foreach ($s as $key1 => $vaule1) {

						$sqlNew .= "`".$key1."` = '".$vaule1."',";
					}
					//最终新的sql语句
					$sqlNew = substr($sqlNew, 0, -1);
					break;

				case 'select':
					if ( isset($sql['field']) && is_array($sql['field']) ) {
						$sqlNew = "SELECT ";
						foreach ($sql['field'] as $value1) {
							$sqlNew .= '`'.$value1.'`,';
						}
						$sqlNew = substr($sqlNew, 0, -1)." FROM `" .$sql['table']. "`";
					} else {
						$sqlNew = "SELECT * FROM `" .$sql['table']. "`";
					}
					break;

				default:
					$this->err("您的 SQL model 不符合规范，请检查！或者请使用原生SQL语句！");
					break;
			}
			//组合出where&limit语句。更新最终新sql
			if ( isset($sql['where']) && is_array($sql['where']) ) {
				foreach ($sql['where'] as $key3 => $value3) {
					$sqlNew .= " WHERE `".$key3."` = '".$value3."' AND";
				}
				$sqlNew = substr($sqlNew, 0, -3);
			}
			if ( isset($sql['limit']) && substr_count($sql['limit'], ',') == 1 ) {
				$sqlNew .= 'LIMIT '.$sql['limit'];
			}

			$result = mysqli_query($this->link, $sqlNew);
			if ( $result ) {
				return $result;
			} else {
				$this->err();
			}
		} elseif ( $sql != '' || $sql != NULL ) {
			$result = mysqli_query($this->link, $sql);
			if ( $result ) {
				return $result;
			} else {
				$this->err();
			}
		} else {
			$this->err("error: the SQL is empty !! Please check the SQL statement!");
			return false;
		}
	}

	/**
	 * 获得一条查询结果
	 * @param  [type] $link [连接标示]
	 * @param  [type] $sql  [sql语句]
	 * @return [type]       [查询成功时返回结果数组]
	 */
	public function getRow($sql){
		$result = $this->query($sql);
		return mysqli_fetch_assoc($result);
	}

	/**
	 * 获得查询结果
	 * @param  [type] $link [连接标示]
	 * @param  [type] $sql  [sql语句]
	 * @return [type]       [查询成功时返回结果数组]
	 */
	public function getAll($sql){
		$result = $this->query($sql);
		while ( $row = mysqli_fetch_assoc($result) ){
			$res[] = $row;
		} 
		return $res;
	}

	/**
	 * 查询select语句的结果数
	 * @param  [type] $link [连接标示]
	 * @param  [type] $sql  [sql语句]
	 * @return [type]       [成功时返回一共的结果。失败时直接输出错误信息]
	 */
	public function num($sql){
		$result = $this->query($sql);
		return mysqli_num_rows($result);
	}

	/**
	 * 查询INSERT, UPDATE, REPLACE or DELETE 所影响的行数
	 * @param  [type] $link [连接标示]
	 * @param  [type] $sql  [sql语句]
	 * @return [type]       [description]
	 */
	public function aff($sql){
		$result = $this->query($sql);
		if ( $result ) {
			return mysqli_affected_rows($this->link);
		} else {
			return 0;
		}
	}

	/**
	 * 获取上一次insert语句得到的auto-incresement字段id值
	 * @param  [type] $link [description]
	 * @return [type]       [description]
	 */
	public function insert_id($sql){
		$result = $this->query($sql);
		if ( $result ) {
			return mysqli_insert_id($this->link);
		} else {
			return 0;
		}
	}

	/**
	 * 关闭上一个mysqli连接
	 * @param  [type] $link [description]
	 * @return [type]       [description]
	 */
	public function close(){
		return mysqli_close($this->link);
	}

	/**
	 * 错误信息
	 * @return [type] [description]
	 */
	public function err($info){
		if ( $info ) {
			echo $info;
		} else {
			echo "DATABASE query error . Debug Info:".mysqli_errno($this->link).":".mysqli_error($this->link);
		}
	}

}